IFS=$'\n'
keymap_liveISO_cfg=($(grep -sv "^#\|^[[:blank:]]*$" "$f_conf_prog_keymap_liveISO"))
if [  ${#keymap_liveISO_cfg[@]} == 1 ] ; then
    loadkeys "$keymap_liveISO_cfg" 2>/dev/null
fi
unset IFS
