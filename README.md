# 0.3.0  
#### INTRO  
Arch installer was created with the aim of saving time and taking advantage of the first available internet connection.  
Arch installer fue creado con el objetivo de ahorrar tiempo y aprovechar la primera coneccion a internet disponible.  

# Please use google or yandex translator if you need another language, it's free!!  

#### CARACTERISTICAS  
- compatible con maquinas virtuales (qemu, virtual box, etc)
- SUDO no habilitado, usted puede habilitarlo mediante el modulo 'comandos adicionales' o manualmente despues del  primer reinicio.
- una configuracion para todos los equipos o uno para cada modelo.
- multiboot linux+linux+linux... o windows+linux+linux+linux...
- instalar gestor de escritorio (gnome, kde, etc) es opcional.
- todos los paquetes son elegibles, tambien puede añadir alguno que crea necesario, como drivers.
- las contraseñas establecidas para la instalacion se eliminan de manera segura sobre escribiendo con ceros.  


#### REPORTE DE ERROR, SOLICITUD DE FUNCIONES, TRADUCCIONES Y DONACIONES  
email: johnca.developer@gmail.com  
reddit: https://www.reddit.com/r/archinstaller/  
paypal: johnca.developer@gmail.com  

#### manual de uso  
https://gitlab.com/johnca2/archinstaller/-/tree/main/static.d/man.d?ref_type=heads  

![DEMO](demo.jpg)

quizas deba leer la guia de instalacion oficial de arch linux para comprender mejor las opciones del programa.  
https://wiki.archlinux.org/title/installation_guide  

# ejecutar el programa:  
`lsblk`  
`mount -m /dev/sdXY /mnt-usb`  
**modo principal --1**  
- el modo principal simplemente usa la configuracion anterio que usted haya realizado.   
`bash /mnt-usb/archinstaller/archinstaller.sh --1` 

**modo dedicado --2**  
- el modo dedicado usa el nombre del equipo para almacenar datos, es decir que si el programa detecta que es la segunda vez que se ejecuta en el mismo equipo (fabricante y modelo), entonces cargara la configuracion especifica que usted haya realizado para ese equipo (fabricante y modelo), la configuracion que haya realizado en varios equipos se almacenan y consultan para cada uno de ellos, evitando que tenga que recordar algun ajuste especifico.  
`bash /mnt-usb/archinstaller/archinstaller.sh --2`

# instalar usando solo una memoria extraible (USB, etc...) RECOMENDADO
se recomeinda el uso de:  
VENTOY (creador USB booteable)  
https://www.ventoy.net/en/download.html  
![ventoy](https://www.ventoy.net/static/img/screen/screen_uefi_en.png?v=4)

GPARTED live-iso (gestor de almacenamiento)  
https://gparted.org/download.php  
![gparted](https://gparted.org/screens/gparted-main-window.png)

- use memdisk mode (si error, entonces debera usar dos almacenamientos USB o la opcion GIT)  
![ventoy-memdisk](https://www.ventoy.net/static/img/secondary_menu1.png)

- si desea iniciar el liveISO y poder ejecutar el programa de instalacion usando una unica memoria USB, use'VENTOY' como asistente para crear un usb booteable, copie la carpeta del programa 'archinstaller' en la primera entrada de la memoria usb (raiz), inicie el liveISO de archlinux en RAM (se encuetra como opcion en la lista antes de iniciar el liveISO), luego **ejecute los siguientes comandos:**  

`lsblk`    
`mount -m /dev/sdXY-id-usb /mnt-usb`  
`bash /mnt-usb/archinstaller/archinstaller.sh`    

# instalar descargando el programa desde el liveISO (posible error al actualizar claves de seguridad pacman 'pacman -Sy')
**ejecute los comandos:**  
### GIT
`pacman -Sy`  
`pacman -S --needed git glibc`  
`git clone https://gitlab.com/johnca2/archinstaller.git`  
`bash archinstaller/archinstaller.sh`  

### w3m  
`pacman -Sy`  
`pacman -S w3m`  
`w3m google.com`  
`search: gitlab johnca2 archinstaller`  
`go to: download source code`  
`select: tar`  
`ls`  
`tar -xvf archinstaller-main.tar`  
`bash archinstaller-main/archinstaller.sh`  




