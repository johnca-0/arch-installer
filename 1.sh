echo -e \
"BIOS date: $bios_date
BIOS release: $bios_release
BIOS vendor: $bios_vendor
BIOS version: $bios_version
BOARD asset tag: $board_asset_tag
BOARD name: $board_name
BOARD serial: $board_serial
BOARD vendor: $board_vendor
BOARD version: $board_version
CHASSIS asset tag: $chassis_asset_tag
CHASSIS serial: $chassis_serial
CHASSIS type: $chassis_type
CHASSIS vendor: $chassis_vendor
CHASSIS version: $chassis_version
EMBEDDED-CONTROLLER firmware release: $ec_firmware_release
PRODUCT family: $product_family
PRODUCT name: $product_name
PRODUCT serial: $product_serial
PRODUCT sku (prodID): $product_sku
PRODUCT uuid: $product_uuid
PRODUCT version: $product_version
SYSTEM vendor: $sys_vendor" > dinamic.d/info.d/info-hw.txt

while true ; do
    cpu=$(cat /proc/cpuinfo 2>/dev/null | grep '^model name.*:' | head -n1 | awk -F: '{print $2}' | sed 's/^[[:space:]]*//g; s/[[:space:]]*$//g')
    if [ -e /sys/firmware/efi/efivars ]; then
        efi_pkg=' efibootmgr'
        type_boot=efi
    else
        type_boot=bios
    fi
    #ram
    ram_kb=$(grep -i memtotal /proc/meminfo | awk '{print $2}')
    ram_gb=$(($ram_kb / 100000))
    if (( $ram_gb < 10 )) ; then
        ram_gb="0$ram_gb"
    fi
    ram_gb=$(sed 's/.$/.&/g' <<< $ram_gb)

    gpu=$(IFS=$'\n'; for i in $(lspci -nn | grep '\[03') ; do printf "($i) " ; done ; unset IFS)
    if [[ `grep -i 'intel' <<< $gpu` ]] ; then
        video_driver=' xf86-video-intel'
    elif [[ `grep -i 'amd' <<< $gpu` ]] ; then
        video_driver=' xf86-video-amdgpu'
    else
		video_driver=''
    fi

	# ZONA HORARIA EN USO -- no 0,2
	IFS=$'\n'
	timezone_cfg=($(grep -sv "^#\|^[[:blank:]]*$" editable.d/"$d_main"/conf.d/timezone.cfg))
	if [ ${#timezone_cfg[@]} == 1 ] ; then
		timezone_cfg_info=$(echo "$(env TZ=$timezone_cfg date +'%:z') $timezone_cfg $(env TZ=$timezone_cfg date +'%F %H:%M')")
	else
		timezone_cfg_info='(0)'
		timedatectl list-timezones | sed 's/^/#/g' >  editable.d/"$d_main"/conf.d/timezone.cfg
	fi
	unset IFS

	# ALMACENAMIENTO PARA LA INSTALACION -- no 0,2
	touch editable.d/"$d_main"/conf.d/storage.cfg dinamic.d/info.d/info-storage.txt
	storage_now=$(lsblk -npo KNAME,SIZE,TYPE,PTTYPE,FSTYPE,FSVER,PARTTYPENAME,ID-LINK)
	if [[ $storage_now != `<dinamic.d/info.d/info-storage.txt` ]] ; then
		printf -- "$storage_now" > dinamic.d/info.d/info-storage.txt
		rm editable.d/"$d_main"/conf.d/storage.cfg
	fi

	IFS=$'\n'
	storage_cfg=($(grep -sv "^#\|^[[:blank:]]*$" editable.d/"$d_main"/conf.d/storage.cfg | awk '{print $1}'))
	dualboot_cfg=($(grep -sv "#dualboot\|^[[:blank:]]*$" editable.d/"$d_main"/conf.d/storage.cfg | awk '{print $1}' | sed 's/^#//g' | grep -vxF "${storage_cfg[*]}" 2>/dev/null))
	dualboot_auto_cfg=($(awk '{print $NF}' editable.d/"$d_main"/conf.d/storage.cfg 2>/dev/null | grep "^auto$"))
	dualboot_cfg_info=$(
		if [[ $dualboot_auto_cfg ]] ; then
		    echo "(dualboot-auto)"
		elif [[ $dualboot_cfg ]] ; then
		    echo "(dualboot)"
		fi)

	if [[ ${#storage_cfg[@]} == 1 &&  `lsblk -ndpo TYPE $storage_cfg` == disk ]] ; then
		mode_storage=1

		if [[ $type_boot == bios ]] ; then
		    part1_info='GRUB = 8 MB'
		else
		    part1_info='/boot/efi = 1 GB'
		fi
		storage_cfg_info="($(lsblk -ndpo TRAN,PATH,SIZE,TYPE,PTTYPE,MODEL "$storage_cfg" | column -t -o ' ')) ($part1_info)"
	elif [[ $type_boot == bios && ${#storage_cfg[@]} == 2 && `lsblk -ndpo TYPE ${storage_cfg[0]}` == part && `lsblk -ndpo TYPE ${storage_cfg[1]}` == part ]] ; then
		mode_storage=bios-2

		dat=$(grep -sv "^#\|^[[:blank:]]*$" editable.d/"$d_main"/conf.d/storage.cfg)
		part_grub=$(grep " grub " <<< $dat | awk '{print $1}')
		part_sys=$(grep " sys " <<< $dat | awk '{print $1}')
		
		if [[ $part_grub && $part_sys ]] ; then 
			if [[ ! `lsblk -ndpo FSTYPE "$part_grub"` && `lsblk -ndpo PARTTYPENAME "$part_grub"` == 'BIOS boot' ]] ; then
				dev_father=$(lsblk -snpo kname "$part_grub" | head -n 1)
				part_grub_info="GRUB: $(lsblk -ndpo PATH,SIZE,TYPE,PTTYPE "$part_grub" | column -t -o ' ')"
				
				storage_cfg_info="(SYS: $(lsblk -ndpo TRAN "$dev_father") $(lsblk -ndpo PATH,SIZE,TYPE,PTTYPE,MODEL "$part_sys" | column -t -o ' ')) ($part_grub_info)"
			else
				storage_cfg_info='(0)'
				echo "error: $part_grub > part-type > BIOS boot ?"
				read -n1
			fi

		else
			storage_cfg_info='(0)'
		fi

		storage_hibernate="$part_sys"
		storage_grub="$dev_father"

	elif [[ $type_boot == efi && ${#storage_cfg[@]} == 2 && `lsblk -ndpo TYPE ${storage_cfg[0]}` == part && `lsblk -ndpo TYPE ${storage_cfg[1]}` == part ]] ; then
		mode_storage=efi-2

		dat=$(grep -sv "^#\|^[[:blank:]]*$" editable.d/"$d_main"/conf.d/storage.cfg)
		dat_efi=$(grep " efi " <<< $dat)
		part_efi=$(awk '{print $1}' <<< "$dat_efi")
		part_sys=$(grep " sys " <<< $dat | awk '{print $1}')
		windows_cfg=$(grep -o " win " <<< "$dat_efi")

		dev_father_efi=$(lsblk -snpo kname $part_efi | head -n 1)
		dev_father_sys=$(lsblk -snpo kname $part_sys | head -n 1)

		if [[ `lsblk -ndpo PARTTYPENAME $part_efi` == 'EFI System' && $part_sys ]]  ; then

		    part_efi_info=$(lsblk -ndpo PATH,SIZE,TYPE,PTTYPE,MODEL,PARTTYPENAME "$part_efi" | column -t -o ' ')
		    part_sys_info=$(lsblk -ndpo PATH,SIZE,TYPE,PTTYPE,MODEL,PARTTYPENAME "$part_sys" | column -t -o ' ')
		    storage_cfg_info="(EFI: $(lsblk -ndpo TRAN "$dev_father_efi") $part_efi_info) (SYS: $(lsblk -ndpo TRAN "$dev_father_sys") $part_sys_info)$(if [[ $windows_cfg ]] ; then printf ' (windows + linux)' ; fi)"
		else
		    storage_cfg_info='(0)'
		    echo "error: $part_efi > part-type > EFI System ?"
		    read -n1
		fi

		storage_hibernate="$part_sys"

	else
		#MODULO: REINICIAR CONFIGURACION
		storage_cfg_info='(0)'
		unset dualboot_cfg_info
		if [[ $type_boot == bios ]] ; then
		    lsblk -e7,11 -npo KNAME,SIZE,TYPE,PTTYPE | sed 's/^/#/g; s/$/ #grub #sys #dualboot #auto/g' | column -t > editable.d/"$d_main"/conf.d/storage.cfg
		else
		    lsblk -e7,11 -npo KNAME,SIZE,TYPE,PTTYPE | sed 's/^/#/g; s/$/ #efi #sys #win #dualboot #auto/g' | column -t > editable.d/"$d_main"/conf.d/storage.cfg
		fi
	fi
	unset IFS

	# SWAPFILE
	touch editable.d/"$d_main"/conf.d/swapfile.cfg dinamic.d/info.d/info-ram.txt
	ram_now=$(grep -i memtotal /proc/meminfo | awk '{print $2,$3}')
	if [[ `wc -w <<< $(<editable.d/"$d_main"/conf.d/swapfile.cfg)` == 0 ]] ; then
		echo "$(($(awk '{print $1}' <<< $ram_now) * 2)) hibernate:0" > editable.d/"$d_main"/conf.d/swapfile.cfg
	fi

	if [[ $ram_now != `<dinamic.d/info.d/info-ram.txt` ]] ; then
		echo "$ram_now" > dinamic.d/info.d/info-ram.txt
		sed -i 's/^[^#]/#&/g' editable.d/"$d_main"/conf.d/swapfile.cfg
		sed -i "1i\\$(( $(awk '{print $1}' <<< $ram_now) * 2)) hibernate:0" editable.d/"$d_main"/conf.d/swapfile.cfg
	fi

	#modulo interpretacion de datos
	touch editable.d/"$d_main"/conf.d/swapfile.cfg
	IFS=$'\n'
	swapfile_cfg=($(grep -sv "^#\|^[[:blank:]]*$" editable.d/"$d_main"/conf.d/swapfile.cfg | awk '{print $1}' | grep -sv '[[:alpha:]]\|[[:punct:]]'))
	hibernate_cfg=$(grep -sv "^#\|^[[:blank:]]*$" editable.d/"$d_main"/conf.d/swapfile.cfg | awk '{print $2}' | awk -F: '{print $2}' | grep -o ^1)
	if [[ "$hibernate_cfg" == 1 ]] ; then
		hibernate_cfg_info='enabled'
	else
		hibernate_cfg_info='disabled'
	fi
	if [[ ${#swapfile_cfg[@]} == 1 ]] ; then
		swapfile_gb_info=$(($swapfile_cfg / 100000))
		if (( $swapfile_gb_info < 10 )) ; then
		    swapfile_gb_info="0$swapfile_gb_info"
		fi
		swapfile_gb_info=$(sed 's/.$/.&/g' <<< $swapfile_gb_info)

		swapfile_cfg_info="(swapfile:$swapfile_cfg kB $swapfile_gb_info gB) (hibernate:$hibernate_cfg_info)"
	else
		sed -i 's/^[^#]/#&/g' editable.d/"$d_main"/conf.d/swapfile.cfg #comentar las lineas no comentadas
		swapfile_cfg_info="(swapfile:disabled) (hibrenate:disabled)"
	fi
	unset IFS

	# PAQUETES BASE -- no 0
	touch editable.d/"$d_main"/conf.d/base-pkg.cfg
	base_pkg_cfg="$(grep -sv "^#\|^[[:blank:]]*$\|^systemctl enable " editable.d/"$d_main"/conf.d/base-pkg.cfg | awk '{print $1}' | tr '\n' ' ' | sed 's/ $//g')$efi_pkg$video_driver"
	base_service_cfg=$(grep "^systemctl enable " editable.d/"$d_main"/conf.d/base-pkg.cfg)
	if [[ `wc -w <<< $(<editable.d/"$d_main"/conf.d/base-pkg.cfg)` == 0 ]] ; then
		cp static.d/default.d/conf.d/base-pkg.cfg editable.d/"$d_main"/conf.d/base-pkg.cfg
		base_pkg_cfg=$(grep -sv "^#\|^[[:blank:]]*$\|^systemctl enable " editable.d/"$d_main"/conf.d/base-pkg.cfg | awk '{print $1}' | tr '\n' ' ' | sed 's/ $//g')
		base_service_cfg=$(grep "^systemctl enable " editable.d/"$d_main"/conf.d/base-pkg.cfg)
	fi


	# GENERAR IDIOMAS -- no 0
	touch editable.d/"$d_main"/conf.d/gen-language.cfg
	IFS=$'\n'
	gen_lang_cfg=($(grep -sv "^#\|^[[:blank:]]*$" editable.d/"$d_main"/conf.d/gen-language.cfg))
	gen_lang_cfg_info=$(for i in ${gen_lang_cfg[@]} ; do printf "($i) " ; done)
	if [[ ! $gen_lang_cfg ]] ; then
		gen_lang_cfg_info='(0)'
		sed 's/^/#/g; s/[[:blank:]]*$//g' /usr/share/i18n/SUPPORTED > editable.d/"$d_main"/conf.d/gen-language.cfg
	fi
	unset IFS

	# IDIOMA DEL NUEVO SISTEMA -- no 0,2
	touch editable.d/"$d_main"/conf.d/language.cfg
	IFS=$'\n'
	language_cfg=($(grep -sv "^#\|^[[:blank:]]*$" editable.d/"$d_main"/conf.d/language.cfg))
	if [[ ${#language_cfg[@]} != 1 ]] ; then
		language_cfg='(0)'
		awk '{print $1}' /usr/share/i18n/SUPPORTED | grep ".UTF-8$" | sed 's/^/#/g' > editable.d/"$d_main"/conf.d/language.cfg
	fi
	unset IFS

	# TECLADO DE CONSOLA -- no 0,2
	touch editable.d/"$d_main"/conf.d/keymap.cfg
	IFS=$'\n'
	keymap_cfg=($(grep -sv "^#\|^[[:blank:]]*$" editable.d/"$d_main"/conf.d/keymap.cfg))
	if [[ ${#keymap_cfg[@]} != 1 ]] ; then
		keymap_cfg='(0)'
		localectl list-keymaps | sed 's/^/#/g' > editable.d/"$d_main"/conf.d/keymap.cfg
	fi
	unset IFS

	# HOSTNAME -- no 0
	touch editable.d/"$d_main"/conf.d/hostname.cfg dinamic.d/info.d/info-product-name.txt
	if [[ `wc -w <<< $(<editable.d/"$d_main"/conf.d/hostname.cfg)` == 0 ]] ; then
		echo "linux-arch-$ID_product" > editable.d/"$d_main"/conf.d/hostname.cfg
	fi
	if [[ $ID_product != `<dinamic.d/info.d/info-product-name.txt` ]] ; then
		echo "$ID_product" > dinamic.d/info.d/info-product-name.txt
		sed -i 's/^[^#]/#&/g' editable.d/"$d_main"/conf.d/hostname.cfg
	fi

	hostname_cfg=($(grep -sv "^#\|^[[:blank:]]*$" editable.d/"$d_main"/conf.d/hostname.cfg | iconv -t ASCII//TRANSLIT | tr '[:punct:]' '-' | tr '[:blank:]' '-'))
	if [[ ${#hostname_cfg[@]} != 1 ]] ; then
		sed -i 's/^[^#]/#&/g' editable.d/"$d_main"/conf.d/hostname.cfg
		sed -i "1i\linux-arch-$ID_product" editable.d/"$d_main"/conf.d/hostname.cfg
		hostname_cfg=$(grep -sv "^#\|^[[:blank:]]*$" editable.d/"$d_main"/conf.d/hostname.cfg | iconv -t ASCII//TRANSLIT | tr '[:punct:]' '-' | tr '[:blank:]' '-')
	fi


	# USUARIOS Y CONTRASEÑAS
	touch editable.d/"$d_main"/conf.d/user-password.cfg
	if [[ `wc -w <<< $(<editable.d/"$d_main"/conf.d/user-password.cfg)` == 0 ]] ; then
		cp static.d/default.d/conf.d/user-password.cfg editable.d/"$d_main"/conf.d/user-password.cfg
	fi
	IFS=$'\n'
	user_passwd_cfg=($(grep -sv "^#\|^[[:blank:]]*$" editable.d/"$d_main"/conf.d/user-password.cfg))
	if [[ ! $user_passwd_cfg ]] ; then
		user_cfg_info='(0)'
	else
		user_passwd_cfg=($(
		    for i in ${user_passwd_cfg[@]} ; do
		        secure_usr=$(awk -F: '{print $1}' <<< "$i" | iconv -t ASCII//TRANSLIT | tr '[:punct:]' '-' | tr '[:blank:]' '-')
		        secure_pwd=$(awk -F: '{print $2}' <<< "$i" | iconv -t ASCII//TRANSLIT | tr '[:punct:]' '-')
		    echo "$secure_usr:$secure_pwd"
		    done))
		user_passwd_cfg_info=$(for i in ${user_passwd_cfg[@]} ; do printf "($i) " ; done)
	fi
	unset IFS

	# ESCRITORIO -- no 2
	cp -u static.d/default.d/desktop.d/* editable.d/"$d_main"/desktop.d
	IFS=$'\n'
	desktop_cfg=($(grep -sv "^#\|^[[:blank:]]*$" editable.d/"$d_main"/conf.d/desktop.cfg))
	if [[ ${#desktop_cfg[@]} != 1 ]] ; then
		desktop_cfg='(0)'
		ls -1 editable.d/"$d_main"/desktop.d | sed 's/^/#/g; s/.sh$//g' > editable.d/"$d_main"/conf.d/desktop.cfg
	fi
	unset IFS

	# FUENTE DE CONSOLA VIRTUAL -- no 2
	touch editable.d/"$d_main"/conf.d/font-vconsole.cfg
	IFS=$'\n'
	font_vconsole_cfg=($(grep -sv "^#\|^[[:blank:]]*$" editable.d/"$d_main"/conf.d/font-vconsole.cfg))
	if [[ ${#font_vconsole_cfg[@]} != 1 ]] ; then
		font_vconsole_cfg='(0)'
		ls -1 /usr/share/kbd/consolefonts | grep '.psfu.gz$' | sed 's/.psfu.gz$//g ; s/^/#/g' >  editable.d/"$d_main"/conf.d/font-vconsole.cfg
	fi
	unset IFS

echo -e \
"$name_prog_system-$version_prog-johnca-$lang_prog_cfg-$keymap_liveISO_cfg
$(printf %"$(tput cols)"s | tr " " "-" ; printf "\n")
id: $ID_product
boot: $type_boot
ram: $ram_kb kB $ram_gb gB
cpu: $cpu
gpu: $gpu
$(printf %"$(tput cols)"s | tr " " "-" ; printf "\n")
5)  $text_timezone (1): $timezone_cfg_info
6)  $text_storage (1) (2): $storage_cfg_info $dualboot_cfg_info
7)  swapfile (kB): $swapfile_cfg_info
8)  $text_base_pkg: $base_pkg_cfg
9)  $text_gen_lang (1) (>1): $gen_lang_cfg_info
10) $text_language (1): $language_cfg
11) $text_keymap (1): $keymap_cfg
12) $text_font_vconsole TTY (0) (1): $font_vconsole_cfg
13) $text_hostname: $hostname_cfg
14) $text_users_passwords (1) (>1): $user_passwd_cfg_info
15) $text_desktop (0) (1): $desktop_cfg" | tee dinamic.d/info.d/info-install.txt

    printf %"$(tput cols)"s | tr " " "-" ; printf "\n"
    echo "$text_menu"
    read answer_installer
    if [[ $answer_installer == '' ]] ; then
		less dinamic.d/info.d/info-install.txt
    elif [[ $answer_installer == 0 ]] ; then
        exit
    elif [[ $answer_installer == 1 ]] ; then
        . ./set-lang.sh
    elif [[ $answer_installer == 2 ]] ; then
		. ./set-keymap.sh
    elif [[ $answer_installer == 3 ]] ; then
        setfont -d /usr/share/kbd/consolefonts/LatArCyrHeb-16.psfu.gz
    elif [[ $answer_installer == 4 ]] ; then
        setfont /usr/share/kbd/consolefonts/LatArCyrHeb-16.psfu.gz
    elif [[ $answer_installer == 5 ]] ; then
		nano editable.d/"$d_main"/conf.d/timezone.cfg
	elif [[ $answer_installer == 6 ]] ; then
		nano editable.d/"$d_main"/conf.d/storage.cfg
	elif [[ $answer_installer == 7 ]] ; then
        nano editable.d/"$d_main"/conf.d/swapfile.cfg
    elif [[ $answer_installer == 8 ]] ; then
		nano editable.d/"$d_main"/conf.d/base-pkg.cfg
    elif [[ $answer_installer == 9 ]] ; then
        nano editable.d/"$d_main"/conf.d/gen-language.cfg
    elif [[ $answer_installer == 10 ]] ; then
        nano editable.d/"$d_main"/conf.d/language.cfg
    elif [[ $answer_installer == 11 ]] ; then
        nano editable.d/"$d_main"/conf.d/keymap.cfg
    elif [[ $answer_installer == 12 ]] ; then
        nano editable.d/"$d_main"/conf.d/font-vconsole.cfg
    elif [[ $answer_installer == 13 ]] ; then
        nano editable.d/"$d_main"/conf.d/hostname.cfg
    elif [[ $answer_installer == 14 ]] ; then
        nano editable.d/"$d_main"/conf.d/user-password.cfg
    elif [[ $answer_installer == 15 ]] ; then
        nano editable.d/"$d_main"/conf.d/desktop.cfg
    elif [[ $answer_installer == 16 ]] ; then
        if [[ $desktop_cfg != '(0)' ]] ;  then
            nano "editable.d/$d_main/desktop.d/$desktop_cfg.sh"
        fi
    elif [[ $answer_installer == 17 ]] ; then
        nano "editable.d/$d_main/cmd-plus.sh"
    elif [[ $answer_installer == 18 ]] ; then
        cat /dinamic.d/info.d/* 2>/dev/null | less
    elif [[ $answer_installer == 19 ]] ; then
        path="editable.d/$d_main/info.d/$ID_product ($product_sku) $(date +'%F') $(date +'%T' | tr '[:punct:]' '.')"
        mkdir -p "$path"
        cp dinamic.d/info.d/* "$path"
        echo "$path"
        read -n1
    elif [[ $answer_installer == 1029 ]] ; then
		rm editable.d/"$d_main"/conf.d/*
        rm editable.d/"$d_main"/desktop.d/*
    elif [[ $answer_installer == 1100 ]] ; then
        ping -c 1 archlinux.org 1>/dev/null 2>/dev/null
        code=$?
        if [[ $code == 0 ]] ; then
            if [[ $storage_cfg_info == '(0)' ]] ; then
                echo 'error: storage'
                read -n1
            else
                break
            fi
        else
            echo 'error: internet'
            read -n1
        fi
    fi
done

. ./root-liveISO.sh
. ./root-maker.sh
cp dinamic.d/root.sh /mnt
arch-chroot /mnt bash /root.sh
. ./secure-remove.sh

swapoff /mnt/swapfile
umount -R /mnt

printf %"$(tput cols)"s | tr " " ":" ; printf "\n"

read -p 'reboot? yes=1 no=0: ' reboot_answer
if [[ "$reboot_answer" == 1 ]] ; then
    systemctl reboot
fi
