echo -e \
"printf %\"\$(tput cols)\"s | tr ' ' '-' ; echo

# TIMEZONE
ln -sf /usr/share/zoneinfo/$timezone_cfg /etc/localtime
hwclock --systohc


# LANGUAGE
$(IFS=$'\n'; for i in ${gen_lang_cfg[@]} ; do
	echo "sed -i 's/^#$i/$i/g' /etc/locale.gen"
done ; unset IFS)
locale-gen
echo 'LANG=$language_cfg' > /etc/locale.conf
echo 'KEYMAP=$keymap_cfg' > /etc/vconsole.conf
echo 'FONT=$font_vconsole_cfg' >> /etc/vconsole.conf
echo '$hostname_cfg' > /etc/hostname


# USER PASSWORD
#chpasswd <<< root:12
$(IFS=$'\n'; for i in ${user_passwd_cfg[@]} ; do
    if [[ $(awk -F: '{print $1}' <<< "$i") == root ]] ; then
        echo "chpasswd <<< '$i'"
    else
        echo "useradd -m '$(awk -F: '{print $1}' <<< "$i")'"
        echo "chpasswd <<< '$i'"
    fi
done ; unset IFS)


# GRUB
$(if [[ $type_boot == bios ]] ; then
    echo "grub-install --target=i386-pc $storage_grub"
else
    echo "grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=GRUB"
fi)
$(if [[ $dualboot_auto_cfg ]] ; then
    ls_storage=$(lsblk -e7 -npo KNAME | grep -vxF "$storage_cfg" | grep -vxF "$(lsblk -e7 -ndpo KNAME)")
    for i in ${ls_storage[@]} ; do
        if [[ ! `lsblk -ndpo MOUNTPOINT "$i"` ]] ; then
            N+=+1
            echo "mount -m $i /mnt$(($N))"
        fi
    done
    echo "sed -i 's/^#GRUB_DISABLE_OS_PROBER=false$/GRUB_DISABLE_OS_PROBER=false/g' /etc/default/grub"
else
    if [[ $dualboot_cfg ]] ; then
        for i in ${dualboot_cfg[@]} ; do
            N+=+1
            echo "mount -m $i /mnt$(($N))"
        done
        echo "sed -i 's/^#GRUB_DISABLE_OS_PROBER=false$/GRUB_DISABLE_OS_PROBER=false/g' /etc/default/grub"
    elif [[ $windows_cfg ]] ; then
        echo "sed -i 's/^#GRUB_DISABLE_OS_PROBER=false$/GRUB_DISABLE_OS_PROBER=false/g' /etc/default/grub"
    fi
fi)
grub-mkconfig -o /boot/grub/grub.cfg
umount -R /mnt*

# SERVICE BASE
$base_service_cfg


# DESKTOP
$(if [[ $desktop_cfg != '(0)' ]] ; then
    cat "editable.d/$d_main/desktop.d/$desktop_cfg.sh"
    if [[ $chassis == vm ]] ; then
        echo -e "\n\npacman -Rsu \$(pacman -Ssq '^xf86-video') 2>/dev/null"
    fi
fi)


# HIBERNATE
$(if [[ $hibernate_cfg == 1 ]] ; then
    echo  \
"line=\$(grep \"^GRUB_CMDLINE_LINUX_DEFAULT=\" /etc/default/grub | sed 's/.$//g')
line_new=\"\$line resume=$storage_hibernate resume_offset=\$(filefrag -v /swapfile | awk '{ if(\$1==\"0:\"){print \$4} }' | tr -d '.')\"
sed -i \"s:^\$line:\$line_new:g\" /etc/default/grub"
    echo "grub-mkconfig -o /boot/grub/grub.cfg"

    echo \
"line=\$(grep \"^HOOKS=\" /etc/mkinitcpio.conf)
line_new=\$(sed 's/ fsck/ resume fsck/g' <<< \$line)
sed -i \"s/^\$line/\$line_new/g\" /etc/mkinitcpio.conf"
    echo \
"kernel_sys=\$(find /boot/vmli*  -printf \"%f\\\n\" | cut -f2- -d -)
IFS=\$'\\\n'; for i in \$kernel_sys ; do
    mkinitcpio -p \$i
done; unset IFS"
fi)


# COMMAND PLUS
$(cat "editable.d/$d_main/cmd-plus.sh" 2>/dev/null)
" > dinamic.d/root.sh
