tmp_file=(
/mnt/root.sh
)
for i in ${tmp_file[@]} ; do
	if [ -e "$i" ] ; then
		bytes_name=$(( $(wc -c <<< "$(basename "$i")") -1))
		rename=$(char='0'; N="$bytes_name"; n=1; while [ $n -le "$N" ] ; do printf -- "$char"; ((++n)); done)
		out_path="$(dirname "$i")/$rename"
		out_num=1
		while [ -e "$out_path" ] ; do
			out_path="${out_path%(*}"
			out_path="$out_path($out_num)"
			((++out_num))
		done
		(head -c "$(wc -c < "$i")" < '/dev/zero' > "$i" && mv "$i" "$out_path" && rm "$out_path")
	fi
done
