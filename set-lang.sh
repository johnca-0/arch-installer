if [[ `wc -w  <<< $(<editable.d/conf.d/lang-prog.cfg)` == 0 ]] ; then
    ls -1 static.d/language.d | sed 's/^/#/g' > editable.d/conf.d/lang-prog.cfg
fi
nano editable.d/conf.d/lang-prog.cfg
. load-lang.sh
