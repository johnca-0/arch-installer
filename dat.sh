bios_date=$(cat /sys/class/dmi/id/bios_date 2>/dev/null) # BIOS ultima actializacion
bios_release=$(cat /sys/class/dmi/id/bios_release 2>/dev/null) # BIOS lanzamiento
bios_vendor=$(cat /sys/class/dmi/id/bios_vendor 2>/dev/null) # BIOS fabricante
bios_version=$(cat /sys/class/dmi/id/bios_version 2>/dev/null) # BIOS version
board_asset_tag=$(cat /sys/class/dmi/id/board_asset_tag 2>/dev/null) # BOARD etiqueta de propiedad
board_name=$(cat /sys/class/dmi/id/board_name 2>/dev/null) #BOARD nombre
board_serial=$(cat /sys/class/dmi/id/board_serial 2>/dev/null 2>/dev/null) # BOARD numero de serie
board_vendor=$(cat /sys/class/dmi/id/board_vendor 2>/dev/null) # BOARD fabricante
board_version=$(cat /sys/class/dmi/id/board_version 2>/dev/null) # BOARD version
chassis_asset_tag=$(cat /sys/class/dmi/id/chassis_asset_tag 2>/dev/null) # CHASSIS etiqueta de propiedad
chassis_serial=$(cat /sys/class/dmi/id/chassis_serial 2>/dev/null 2>/dev/null) # CHASSIS numero de  serie
chassis_type=$(cat /sys/class/dmi/id/chassis_type 2>/dev/null) # CHASSIS tipo
chassis_vendor=$(cat /sys/class/dmi/id/chassis_vendor 2>/dev/null) # CHASSIS fabricante
chassis_version=$(cat /sys/class/dmi/id/chassis_version 2>/dev/null) # CHASSIS version
ec_firmware_release=$(cat /sys/class/dmi/id/ec_firmware_release 2>/dev/null) #CONTROLADOR EMBEBIDO firmware lanzamiento
product_family=$(cat /sys/class/dmi/id/product_family 2>/dev/null) # PRODUCT familia
product_name=$(cat /sys/class/dmi/id/product_name 2>/dev/null) # PRODUCT nombre
product_serial=$(cat /sys/class/dmi/id/product_serial 2>/dev/null) #PRODUCT numero de serie
product_sku=$(cat /sys/class/dmi/id/product_sku 2>/dev/null) # PRODUCT identidad del producto
product_uuid=$(cat /sys/class/dmi/id/product_uuid 2>/dev/null) #PRODUCT uuid
product_version=$(cat /sys/class/dmi/id/product_version 2>/dev/null) # PRODUCT
sys_vendor=$(cat /sys/class/dmi/id/sys_vendor 2>/dev/null) # SYSTEM fabricante

ID_product=$(
if [[ ! `grep -i "$sys_vendor" <<< "$product_name"` ]] ; then
    echo "$sys_vendor $product_name"
else
    echo "$product_name"
fi | sed 's/^[[:space:]]*//g; s/[[:space:]]*$//g')

d_base=(
dinamic.d/info.d
editable.d/conf.d
static.d/language.d
static.d/man.d
static.d/icon.d
static.d/default.d/conf.d
static.d/default.d/desktop.d
)

for i in ${d_base[@]} ; do mkdir -p "$i" ; done
