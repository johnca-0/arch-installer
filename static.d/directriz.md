# estructura
*dinamic*
    - archivos que son escritos en cada inicio del programa que  por alguna razon no pueden ser lamacenados como variables, extremadamente util cuando se requiere informacion actualizada para disparar alguna accion. es la principal fuente de entrada de informacion actualizada.
    - lo mas probable es que estos archivos se almacenen en la mamoria ram, bajo la ruta: /dev/shm/nameprog-pid
    - pues la velocidad de escritura y lectura requerida por el programa puede ser muy alta.
    - si la carpeta queda vacia, almenos imprimir una lista de los archivos creados en: ls -1 /dev/shm | grep "^nameprog-pid"

*editable*
    * prog
    archivos escritos por el programa, proposito comun:
    - conf.d - almacenar configuracion del programa
    - backup

    * user/main
    archivos que pueden ser creados y editados por el programa solo si no existe o si la informacion necesita ser actualizada, pero en la mayoria de los casos se conserva los cambios hechos por el usuario, es la principal fuente de entrada de peticiones para el ususario.
    - conf.d - almacenar peticiones del usuario (funcion principal, proposito)

*static*
    archivos utiles para el programa que no deberian ser modificados pues probocaria un mal funcionamiento, probablemente estos archivos no se puedan reconstruir pues al no requerir cambios suponemos que siempre estan y estaran donde les corresponde o su reconstruccion seria imposible debido a su naturaleza, estos archivos pueden ser:
    * man
    * lang
    * icon
    * default: archivos que seran copiados para el usuario en caso de ser necesario o solicitado.
