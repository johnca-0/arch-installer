# BASE
pkg=(
gnome
gst-libav
gnome-browser-connector
)
pacman -S --noconfirm ${pkg[@]}



# EXTRA
pkg=(
vlc
firefox
libreoffice-fresh
#filezilla
#libreoffice-fresh-es
#firefox-i18n-es-es
#libreoffice-fresh-ru
#firefox-i18n-ru-ru
)
pacman -S --noconfirm ${pkg[@]}



# SERVICE
systemctl enable gdm
systemctl enable bluetooth