# BASE
pkg=(
lxqt
lightdm
light-locker
#xscreensaver
)
pacman -S --noconfirm ${pkg[@]}



# NECESSARY
pkg=(
accountsservice
breeze
breeze-icons
oxygen-icons
kate
leafpad
gnu-free-fonts
lightdm-gtk-greeter
xorg-server-xephyr
lightdm-gtk-greeter-settings
gvfs
gvfs-mtp
htop
network-manager-applet
okular
copyq
leafpad
p7zip
kcalc
alsa-utils
)
pacman -S --noconfirm ${pkg[@]}



# ACCESSORIES
pkg=(
bluez
bluez-utils
blueman
)
pacman -S --noconfirm ${pkg[@]}



# EXTRA
pkg=(
vlc
firefox
libreoffice-fresh
#filezilla
#libreoffice-fresh-es
#firefox-i18n-es-es
#libreoffice-fresh-ru
#firefox-i18n-ru-ru
)
pacman -S --noconfirm ${pkg[@]}



# SERVICE
systemctl enable lightdm
systemctl enable bluetooth