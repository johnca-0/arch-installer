# BASE
pkg=(
cinnamon
lightdm
lightdm-gtk-greeter
cinnamon-translations
gnome-terminal
xorg
)
pacman -S --noconfirm ${pkg[@]}



# NECESSARY
pkg=(
gnome-system-monitor
)
pacman -S --noconfirm ${pkg[@]}



# EXTRA
pkg=(
vlc
firefox
libreoffice-fresh
#filezilla
#libreoffice-fresh-es
#firefox-i18n-es-es
#libreoffice-fresh-ru
#firefox-i18n-ru-ru
)
pacman -S --noconfirm ${pkg[@]}

# SERVICE
systemctl enable lightdm
