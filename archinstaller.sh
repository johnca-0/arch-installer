#!/usr/bin/env bash
#2024-01-31

name_prog_system='archinstaller'
version_prog='0.3.0'

pid0=$$
dir0=$(dirname "$(readlink -f "$0")")
cd "$dir0"
printf %"$(tput cols)"s | tr " " ":" ; printf "\n"
setfont /usr/share/kbd/consolefonts/LatArCyrHeb-16.psfu.gz 2>/dev/null

permissions(){
chmod -R ugo=rw,-x+X --preserve-root "$dir0"
}
trap 'permissions' EXIT


. ./dat.sh
. ./load-lang.sh

if [[ $1 == '' ]] ; then
	if [ -e "static.d/man.d/$lang_prog_cfg" ] ; then
		less "static.d/man.d/$lang_prog_cfg"
		while true; do
			echo "1) $text_program_language (1): $lang_prog_cfg"
			echo "2) $text_keymap_liveISO (1): $keymap_liveISO_cfg"
			printf %"$(tput cols)"s | tr " " "-" ; printf "\n"
			echo "$text_menu_man"
			read answer_man
			if [[ $answer_man == 0 ]] ; then
				exit
			elif [[ $answer_man == 1 ]] ; then
				. ./set-lang.sh
			elif [[ $answer_man == 2 ]] ; then
				. ./set-keymap.sh
			elif [[ $answer_man == 3 ]] ; then
				setfont -d /usr/share/kbd/consolefonts/LatArCyrHeb-16.psfu.gz
			elif [[ $answer_man == 4 ]] ; then
				setfont /usr/share/kbd/consolefonts/LatArCyrHeb-16.psfu.gz
			elif [[ $answer_man == 11 ]] ; then
				less --help
			elif [[ $answer_man == 22 ]] ; then
				nano --help | less
			elif [[ $answer_man == '' ]] ; then
				less "static.d/man.d/$lang_prog_cfg"
			fi
		done
	else
		if [ -e "static.d/man.d/english" ] ; then
			less "static.d/man.d/english"
		else
			echo 'error: manual'
			exit
		fi
	fi

elif [[ $1 == --1 ]] ; then
	if [[ $EUID == 0 && `</etc/hostname` == archiso ]] ; then
		d_main=main.d
		d_base=("editable.d/$d_main/conf.d" "editable.d/$d_main/desktop.d")
		for i in ${d_base[@]} ; do mkdir -p "$i" ; done
		. ./1.sh || exit
	else
		echo 'error: root live-iso'
		read -n1
	fi
elif [[ $1 == --2 ]] ; then
	if [[ $EUID == 0 && `</etc/hostname` == archiso ]] ; then
		d_main="$(tr '[:punct:]' '-' <<< "$ID_product" | tr '[:blank:]' '-').d"
		d_base=("editable.d/$d_main/conf.d" "editable.d/$d_main/desktop.d")
		for i in ${d_base[@]} ; do mkdir -p "$i" ; done
		. ./1.sh || exit
	else
		echo 'error: root live-iso'
		read -n1
	fi
fi

chmod -R ugo=rw,-x+X --preserve-root "$dir0"
