if [[ $mode_storage == 1 ]] ; then
    if [[ $type_boot == bios ]] ; then
        echo -e 'g\nn\n\n\n+8m\nt\n4\nn\n\n\n\nt\n\n20\nw\n' | fdisk "$storage_cfg"

        dev_father="$storage_cfg"
        father=$(basename "$dev_father")
        dev_part=$(lsblk -pno kname "$dev_father" | tail -n 1)
        part=$(basename "$dev_part")
        separator=$(sed "s/^$father//g; s/[[:digit:]]*$//g" <<< $part)

        storage_grub="$storage_cfg"
        storage_hibernate="$storage_cfg$separator"2

        echo y | mkfs.ext4 "$storage_cfg$separator"2
        mount "$storage_cfg$separator"2 /mnt

    else

        echo -e 'g\nn\n\n\n+1g\nt\n1\nn\n\n\n\nt\n\n23\nw\n' | fdisk "$storage_cfg"

        dev_father="$storage_cfg"
        father=$(basename "$dev_father")
        dev_part=$(lsblk -pno kname "$dev_father" | tail -n 1)
        part=$(basename "$dev_part")
        separator=$(sed "s/^$father//g; s/[[:digit:]]*$//g" <<< $part)

        storage_hibernate="$storage_cfg$separator"2

        mkfs.fat -F 32 "$storage_cfg$separator"1
        echo y | mkfs.ext4 "$storage_cfg$separator"2

        mount "$storage_cfg$separator"2 /mnt
        mount -m "$storage_cfg$separator"1 /mnt/boot/efi

    fi
elif [[ $mode_storage == bios-2 ]] ; then
    echo y | mkfs.ext4 "$part_sys"
    mount "$part_sys" /mnt
elif [[ $mode_storage == efi-2 ]] ; then
    if [[ ! $windows_cfg ]] ; then
        mkfs.fat -F 32 "$part_efi"
    fi
    echo y | mkfs.ext4 "$part_sys"
    mount "$part_sys" /mnt
    mount -m "$part_efi" /mnt/boot/efi
fi

if [[ $swapfile_cfg ]] ; then
    dd if=/dev/zero of=/mnt/swapfile bs=1K count=$swapfile_cfg status=progress
    chmod 0600 /mnt/swapfile
    mkswap -U clear /mnt/swapfile
    swapon /mnt/swapfile
fi

timedatectl set-timezone $timezone_cfg
reflector --verbose --latest 5 --sort rate --save /etc/pacman.d/mirrorlist
pacstrap -K /mnt $base_pkg_cfg
genfstab -U /mnt >> /mnt/etc/fstab
